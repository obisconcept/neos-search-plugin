<?php

namespace ObisConcept\NeosSearchPlugin\Controller;

use Neos\ContentRepository\Eel\FlowQueryOperations\FindOperation;
use Neos\Eel\FlowQuery\FlowQuery;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\View\ViewInterface;
use Neos\FluidAdaptor\View\TemplateView;
use Neos\Neos\Service\LinkingService;

/**
 * Class IndexedSearchController
 *
 * @package ObisConcept\NeosSearchPlugin
 * @subpackage Controller
 */
class IndexedSearchController extends \Neos\Flow\Mvc\Controller\ActionController
{

    /**
     * Linking service
     *
     * @Flow\Inject
     * @var LinkingService
     */
    protected $linkingService;

    /**
     * Workspace repository
     *
     * @Flow\Inject
     * @var \Neos\ContentRepository\Domain\Repository\WorkspaceRepository
     */
    protected $workspaceRepository;

    /**
     * NodeData repository
     *
     * @Flow\Inject
     * @var \Neos\ContentRepository\Domain\Repository\NodeDataRepository
     */
    protected $nodeDataRepository;

    /**
     * Node factory
     *
     * @Flow\Inject
     * @var \Neos\ContentRepository\Domain\Factory\NodeFactory
     */
    protected $nodeFactory;

    /**
     * Context factory
     *
     * @Flow\Inject
     * @var \Neos\Neos\Domain\Service\ContentContextFactory
     */
    protected $contextFactory;

    /**
     * Indexed search service
     *
     * @Flow\Inject
     * @var \ObisConcept\NeosSearchPlugin\Domain\Service\IndexedSearchService
     */
    protected $indexedSearchService;

    /**
     * Settings
     *
     * @var array
     */
    protected $settings;

    /**
     * Inject settings
     *
     * @param array $settings
     */
    public function injectSettings(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Initializes the view before invoking an action method
     *
     * @param ViewInterface $view The view to be initialized
     * @return void
     */
    protected function initializeView(ViewInterface $view)
    {
        if ($view instanceof TemplateView) {
            $view->setOption('templateRootPaths', [$this->settings['templateRootPath']]);
        }
    }

    /**
     * Search box
     *
     * @return void
     */
    public function indexAction()
    {
        $searchResultNode = $this->request->getInternalArgument('__searchResultNode');

        if (!$searchResultNode) {
            $searchResultIdentifier = $this->request->getInternalArgument('__searchResultIdentifier');
            if ($searchResultIdentifier != '') {
                $siteNode = $this->request->getInternalArgument('__documentNode')->getContext()->getCurrentSiteNode();
                $flowQuery = new FlowQuery(array($siteNode));
                $operation = new FindOperation();
                $operation->evaluate($flowQuery, array('#' . $searchResultIdentifier));
                $searchResultNode = (isset($flowQuery->getContext()[0]) && $flowQuery->getContext()[0] != null) ? $flowQuery->getContext()[0] : null;
            }
        }

        $this->view->assign(
            'searchResultNode',
            $searchResultNode
        );

        $this->view->assign(
            'documentNode',
            $this->request->getInternalArgument('__documentNode')
        );
    }

    /**
     * Search for all properties in node for given term
     *
     * @return void
     */
    public function searchResultAction()
    {
        $searchArguments = $this->request->getHttpRequest()->getArgument('--obisconcept_neossearchplugin-indexedsearch');
        if ($searchArguments === null) {
            $searchArguments = $this->request->getHttpRequest()->getArgument('--neos_nodetypes-page');
        }

        $searchParameter = $searchArguments['searchParameter'];
        $currentNode = $this->request->getInternalArgument('__documentNode');

        if ($searchParameter !== null && $searchParameter !== '') {
            $searchResults = $this->indexedSearchService->search($searchParameter, $currentNode);
            $this->view->assignMultiple(array('searchResults' => $searchResults, 'searchParameter' => $searchParameter));
        }
    }

    /**
     * Search for all properties in node for given term for AJAX requests
     *
     * @return void
     */
    public function searchResultAjaxAction()
    {
        $results = array();
        $searchArguments = $this->request->getArguments();
        if (isset($searchArguments['searchParameter']) && isset($searchArguments['currentNodePath'])) {
            $searchParameter = $searchArguments['searchParameter'];
            $currentNodePath = $searchArguments['currentNodePath'];

            $liveWorkspace = $this->workspaceRepository->findOneByName('live');
            $nodeData = $this->nodeDataRepository->findOneByPath($currentNodePath, $liveWorkspace);
            $context = $this->contextFactory->create(array('targetDimensions' => array('language' => 'de')));
            $currentNode = $this->nodeFactory->createFromNodeData($nodeData, $context);

            if ($searchParameter !== null && $searchParameter !== '') {
                $counter = 1;
                $searchResults = $this->indexedSearchService->search($searchParameter, $currentNode);

                if ($searchResults) {
                    foreach ($searchResults as $searchResult) {
                        if ($counter <= 10) {
                            $result = array();
                            $result['title'] = $searchResult['documentNode']->getProperty('title');
                            $result['findString'] = $searchResult['findString'];
                            $result['uri'] = $this->linkingService->createNodeUri($this->getControllerContext(), $searchResult['documentNode'], $currentNode, 'html', true);
                            array_push($results, $result);
                        }

                        $counter++;
                    }
                }
            }
        }

        $this->view->assign('value', array('results' => $results));
    }
}
