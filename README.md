# Neos CMS - Search Plugin
A small node type based search plugin for TYPO3 Neos.

## Installation
Add the package in your site package composer.json
```
"require": {
    "obisconcept/neos-search-plugin": "~1.0"
}
```

Add these lines to the main Routes.yaml before TYPO3 Neos subroutes definition:
```
-
  name: 'ObisConceptNeosSearchPlugin'
  uriPattern: '<ObisConceptNeosSearchPluginSubroutes>'
  subRoutes:
    'ObisConceptNeosSearchPluginSubroutes':
      package: 'ObisConcept.NeosSearchPlugin'
```

## Version History

### Changes in 1.1.1
- Bugfix in index template

### Changes in 1.1.0
- Bugfixing and refactoring of ajax

### Changes in 1.0.4
- Added search tags in Document NodeType

### Changes in 1.0.3
- Optimized ajax search

### Changes in 1.0.2
- Optimized search engine

### Changes in 1.0.1
- Basic Json integration for ajax search

### Changes in 1.0.0
- First version of the Neos CMS search plugin